# my-second-django-tutorial
This is the code for a tutorial I created about Django for beginners, with special focus on Windows.
You can find this tutorial and more like it on my personal website http://lanigouws.com

#####To make it work

- You need to create a virtualenv with:
  `mkvirtualenv name`
- Then install the required packages, into the virtualenv, with:
`pip install -r requirements.txt`
- Add a SECRET_KEY in **settings.py**
- Create a MySQL database and add the details to the DATABASES setting in **settings.py**
- Then `python manage.py migrate`
- Create a superuser for the admin site: `python manage.py createsuperuser`
- And `python manage.py runserver` to activate the development server.